import { createContext, ReactNode, useContext, useState } from "react";
import { toast } from "react-toastify";
import { api } from "../services/api";
import { Product, Stock } from "../types";

interface CartProviderProps {
  children: ReactNode;
}

interface UpdateProductAmount {
  productId: number;
  amount: number;
}

interface CartContextData {
  cart: Product[];
  addProduct: (productId: number) => Promise<void>;
  removeProduct: (productId: number) => void;
  updateProductAmount: ({ productId, amount }: UpdateProductAmount) => void;
}

const CartContext = createContext<CartContextData>({} as CartContextData);

const checkStock = async function (
  productId: number,
  amount: number
): Promise<boolean> {
  const stock = await api.get<Stock>(`/stock/${productId}`);

  if (stock.data.amount < amount) {
    toast.error("Quantidade solicitada fora de estoque");
    return false;
  }

  return true;
};

export function CartProvider({ children }: CartProviderProps): JSX.Element {
  const [cart, setCart] = useState<Product[]>(() => {
    const storagedCart = localStorage.getItem("@RocketShoes:cart") || "[]";

    return JSON.parse(storagedCart);
  });

  const addProduct = async (productId: number) => {
    try {
      const newCart = [...cart];
      const cartHasProduct = newCart.filter(
        (product) => product.id === productId
      );

      if (!cartHasProduct.length) {
        const stock = await checkStock(productId, 1);

        if (!stock) {
          return;
        }

        const product = await api.get<Product>(`/products/${productId}`);

        newCart.push({ ...product.data, amount: 1 });

        localStorage.setItem("@RocketShoes:cart", JSON.stringify(newCart));

        setCart(newCart);
      } else {
        const productIndex = cart.findIndex(
          (product) => product.id === productId
        );

        const product = cart[productIndex];

        updateProductAmount({ productId, amount: product.amount + 1 });
      }
    } catch {
      toast.error("Erro na adição do produto");
    }
  };

  const removeProduct = (productId: number) => {
    try {
      const productIndex = cart.findIndex(
        (product) => product.id === productId
      );

      if (productIndex === -1) {
        throw Error();
      }

      const updatedCart = [...cart];

      updatedCart.splice(productIndex, 1);

      localStorage.setItem("@RocketShoes:cart", JSON.stringify(updatedCart));

      setCart(updatedCart);
    } catch {
      toast.error("Erro na remoção do produto");
    }
  };

  const updateProductAmount = async ({
    productId,
    amount,
  }: UpdateProductAmount) => {
    if (amount < 1) {
      return;
    }

    try {
      const updatedCart = [...cart];

      const productIndex = updatedCart.findIndex(
        (product) => product.id === productId
      );

      const product = updatedCart[productIndex];

      const stock = await checkStock(productId, amount);

      if (!stock) {
        return;
      }

      product.amount = amount;

      localStorage.setItem("@RocketShoes:cart", JSON.stringify(updatedCart));

      setCart(updatedCart);
    } catch {
      toast.error("Erro na alteração de quantidade do produto");
    }
  };

  return (
    <CartContext.Provider
      value={{ cart, addProduct, removeProduct, updateProductAmount }}
    >
      {children}
    </CartContext.Provider>
  );
}

export function useCart(): CartContextData {
  const context = useContext(CartContext);

  return context;
}
